package RocketMan.gamestate;

import RocketMan.main.GamePanel;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class MenuState extends GameState {

    public MenuState(GameStateManager gsm) {
        super(gsm);
    }

    public void init() {}

    public void tick() {

    }

    public void draw(Graphics g) {
        g.setColor(Color.BLACK);
        g.setFont(new Font("Arial", Font.PLAIN, 64));
        g.drawString("START!", GamePanel.WIDTH/2 - 128, GamePanel.HEIGHT/2 - 64);
    }

    public void keyPressed(int k) {
        if(k == KeyEvent.VK_SPACE) {
            gsm.states.push(new LevelState(gsm));
        }
    }

    public void keyReleased(int k) {

    }

    public void mousePressed(MouseEvent e) {

    }
}
