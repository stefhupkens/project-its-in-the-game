package RocketMan.gamestate;

import RocketMan.main.GamePanel;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class GameoverState extends GameState {

    private int score;

    public GameoverState(GameStateManager gsm, int score) {
        super(gsm);
        this.score = score;
    }

    public void init() {}

    public void tick() {

    }

    public void draw(Graphics g) {
        g.setColor(Color.BLACK);
        g.setFont(new Font("Arial", Font.PLAIN, 64));
        g.drawString("SCORE: " + score, GamePanel.WIDTH/2 - 192, GamePanel.HEIGHT/2 - 96);
        g.drawString("GAME OVER!", GamePanel.WIDTH/2 - 192, GamePanel.HEIGHT/2);
        g.drawString("EXIT", GamePanel.WIDTH - 192, 64);
    }

    public void keyPressed(int k) {
        if(k == KeyEvent.VK_SPACE) {
            gsm.states.pop();
            gsm.states.pop();
            gsm.states.push(new LevelState(gsm));
        }
    }

    public void keyReleased(int k) {

    }

    public void mousePressed(MouseEvent e) {
        int mx = e.getX();
        int my = e.getY();
        if (mx <= 1560 && mx >= 1400 && my <= 70 && my >= 10) {
            gsm.states.pop();
            gsm.states.pop();
        }
    }
}

