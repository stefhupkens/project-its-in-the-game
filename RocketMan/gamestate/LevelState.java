package RocketMan.gamestate;

import RocketMan.entities.Ground;
import RocketMan.entities.Player;
import RocketMan.entities.Rocket;
import RocketMan.main.GamePanel;

import java.awt.*;
import java.awt.event.MouseEvent;

public class LevelState extends GameState {

    private Player player;
    private Ground ground;
    private Rocket rocket0, rocket1;
    private int score;

    public LevelState(GameStateManager gsm) {
        super(gsm);
    }

    public void init() {
        player = new Player(90,150);
        ground = new Ground();
        rocket0 = new Rocket(2);
        rocket1 = new Rocket(1);
        score = 0;
    }

    public void tick() {
        score++;
        player.tick(ground);
        if(rocket0.tick(player) || rocket1.tick(player)) {
            gameOver(score);
        }
    }

    public void draw(Graphics g) {
        ground.draw(g);
        g.setFont(new Font("Arial", Font.PLAIN, 64));
        g.drawString("EXIT", GamePanel.WIDTH - 192, 64);
        g.drawString("SCORE: " + Integer.toString(score),32,64);
        player.draw(g);
        rocket0.draw(g);
        rocket1.draw(g);
    }

    public void gameOver(int score) {
        gsm.states.push(new GameoverState(gsm, score));
    }

    public void keyPressed(int k) {
        player.keyPressed(k);
    }

    public void keyReleased(int k) {
        player.keyReleased(k);
    }

    public void mousePressed(MouseEvent e) {
        int mx = e.getX();
        int my = e.getY();
        if (mx <= 1560 && mx >= 1400 && my <= 70 && my >= 10) {
            gameOver(score);
        }
    }
}
