package RocketMan.entities;

import RocketMan.main.GamePanel;
import RocketMan.physics.Collision;

import java.awt.*;
import java.util.Random;

public class Rocket extends Rectangle {

    private static final long serialVersionUID = 1L;

    private Random r;

    public static final int rocketSize = 50;

    public int x, y;

    private double speed = 0;
    private double currentSpeed = speed;

    private double speedChange;

    public Rocket(int speedChange) {
        x = GamePanel.WIDTH;
        y = GamePanel.HEIGHT/8*6;
        setBounds(x, y, rocketSize*3, rocketSize);
        r = new Random();
        this.speedChange = speedChange;
    }

    public boolean tick(Player player) {
        if (x > 0 - rocketSize*4) {
            x -= currentSpeed;

            currentSpeed += speedChange;
        }

        if (x <= 0 - rocketSize*4) {
            x = GamePanel.WIDTH;
            y = player.y + rocketSize/4;
            currentSpeed = speed + r.nextInt(5);
        }

        if(Collision.rocketPlayer(new Rectangle(x, y, rocketSize*3, rocketSize), new Rectangle(player.x, player.y, player.width, player.height))) {
            return true;
        }

        return false;
    }

    public void draw(Graphics g) {
        g.setColor(Color.RED);
        g.fillRect(x, y, rocketSize*3, rocketSize);
    }

}
