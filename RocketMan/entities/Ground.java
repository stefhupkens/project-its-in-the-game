package RocketMan.entities;

import RocketMan.main.GamePanel;

import java.awt.*;

public class Ground extends Rectangle {
    private static final long serialVersionUID = 1L;

    public static final int groundWidth = GamePanel.WIDTH;
    public static final int groundHeight = GamePanel.HEIGHT/8;

    public Ground() {
        setBounds(0, (int)(groundHeight * 7.5), groundWidth, groundHeight);
    }

    public void draw(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(x, y, groundWidth, groundHeight);
    }
}
