package RocketMan.entities;

import RocketMan.main.GamePanel;
import RocketMan.physics.Collision;

import java.awt.*;
import java.awt.event.KeyEvent;

public class Player extends Rectangle {

    private static final long serialVersionUID = 1L;

    private boolean jumping = false, falling = true;

    private int jumped = 0;

    public int x, y;
    public int width, height;

    private double jumpSpeed = 30;
    private double currentJumpSpeed = jumpSpeed;

    private double maxFallSpeed = 30;
    private double currentFallSpeed = 0;

    public Player(int width, int height) {
        x = GamePanel.WIDTH/12;
        y = GamePanel.HEIGHT/8 * 6;
        this.width = width;
        this.height = height;
    }

    public void tick(Ground gr) {

        if(jumping) {
            y -= currentJumpSpeed;

            currentJumpSpeed -= 1;

            if(currentJumpSpeed <= 0) {
                currentJumpSpeed = jumpSpeed;
                jumping = false;
                falling = true;
            }
        }

        if(falling) {
            y += currentFallSpeed;

            if(currentFallSpeed < maxFallSpeed) {
                currentFallSpeed += 1;
            }
        }

        if(!falling) {
            currentFallSpeed = 0;
        }

        if(Collision.playerGround(new Point(x,y + height), gr)) {
            falling = false;
            jumped = 0;
            y = (int)(gr.groundHeight * 7.5) - height;
        }

    }

    public void draw(Graphics g) {
        g.setColor(Color.GREEN);
        g.fillRect(x, y, width, height);
    }

    public void keyPressed(int k) {
        if(k == KeyEvent.VK_SPACE && jumped < 2) {
            jumped++;
            jumping = true;
        }
    }

    public void keyReleased(int k) {

    }

}
