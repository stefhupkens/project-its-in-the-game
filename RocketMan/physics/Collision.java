package RocketMan.physics;

import RocketMan.entities.Ground;

import java.awt.*;

public class Collision {

    public static boolean playerGround(Point p, Ground g) {
        return g.contains(p);
    }

    public static boolean rocketPlayer(Rectangle r, Rectangle p) {
       return r.intersects(p);
    }
}
